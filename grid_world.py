import numpy as np
import matplotlib.pyplot as plt
import warnings

warnings.filterwarnings("ignore")

##
def gen_grid_world():
    """
    The gen_grid_world function creates a grid world environment.
    The function returns the grid_world and a dictionary that maps each location to an integer or string.
    The dictionary is used for visualization purposes.

    :return: grid_world is a numpy array (9, 10) the grid world and grid_dict is a dictionary that can be used
    to convert from numbers to letters
    """
    O = -1e5  # Dangerous places to avoid
    D = 35  # Dirt
    W = -100  # Water
    C = -3000  # Cat
    T = 1000  # Toy
    grid_dict = {0: '', O: 'O', D: 'D', W: 'W', C: 'C', T: 'T'}
    grid_world = np.array([[0, 0, 0, O, 0, O, 0, 0, 0, 0],  # 1st row of grid (from above to below)
                           [0, 0, 0, O, D, O, 0, 0, D, 0],  # 2nd row of grid (from above to below)
                           [0, 0, 0, O, 0, O, 0, 0, 0, 0],  # 3rd
                           [O, O, O, O, 0, O, O, O, O, O],  # 4th
                           [D, 0, 0, D, 0, O, T, D, 0, 0],  # 5th
                           [0, O, D, D, 0, O, W, 0, 0, 0],  # 6th
                           [W, O, 0, O, 0, O, D, O, O, 0],  # 7th
                           [W, 0, 0, O, D, 0, 0, O, D, 0],  # 8th
                           [0, 0, 0, D, C, O, 0, 0, D, 0]])  # 9th
    return grid_world, grid_dict

##
def show_world(grid_world, tlt):  # shows grid world without annotations
    """
    The show_world function takes a grid world and its title as arguments.
    It plots the grid world without any annotations, i.e., only the cells themselves are shown,
    and it returns an axis object that can be used to plot additional information on top of this grid.

    :param grid_world: A numpy array (9, 10) the grid world
    :param tlt: string to set the title of the plot
    :return: The plot of the grid world without annotations
    """
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title(tlt)
    ax.set_xticks(np.arange(10))  # xticks from 0.5 to 9.5
    ax.set_yticks(np.arange(10))  # yticks from 0.5 to 8.5
    for i in range(10):
        ax.plot([i - 0.5, i - 0.5], [-0.5, 10.5], color="b", linestyle="-", linewidth=1)
        ax.plot([-0.5, 10.5], [i - 0.5, i - 0.5], color="b", linestyle="-", linewidth=1)
    ax.imshow(grid_world, interpolation="nearest", cmap="hot")
    return ax


##
def show_text_state(grid_world, grid_dict, ax):  # shows annotations
    """
    The show_text_state function displays the state of each grid cell in a text format.
    The function takes three arguments:
        - grid_world: A numpy array (9, 10) The current states of the world.
        - grid_dict: A list containing all possible states and their corresponding integers.
        - ax: The subplot object that is being used to display the GridWorld.

    :param grid_world: Get the shape of the grid
    :param grid_dict: a dictionary shows the text of each cell
    :param ax: Draw the annotations on the given axes
    :return: The annotations
    """
    for x in range(grid_world.shape[0]):  # for each row
        for y in range(grid_world.shape[1]):  # for each column
            if grid_world[x, y] >= -3000:  # Dirt, Water, Cat and Toy
                ax.annotate(
                    grid_dict[(grid_world[x, y])],
                    xy=(y, x),
                    horizontalalignment="center",
                )

##
def show_policy(policy, ax):
    """
    The show_policy function takes a policy and plots it on the grid.
    It uses the arrow characters to indicate which action is associated with each policy.

    :param policy: A numpy array (9, 10) determines which action to take in each state
    :param ax: Draw the policy on the grid
    :return: A visualization of the policy
    """
    print(type(policy), policy.shape)
    for x in range(policy.shape[0]):
        for y in range(policy.shape[1]):
            if policy[x, y] == 0:
                ax.annotate(r"$\downarrow$", xy=(y, x), horizontalalignment="center")
            elif policy[x, y] == 1:
                ax.annotate(r"$\rightarrow$", xy=(y, x), horizontalalignment="center")
            elif policy[x, y] == 2:
                ax.annotate(r"$\uparrow$", xy=(y, x), horizontalalignment="center")
            elif policy[x, y] == 3:
                ax.annotate(r"$\leftarrow$", xy=(y, x), horizontalalignment="center")
            elif policy[x, y] == 4:
                ax.annotate(r"$\perp$", xy=(y, x), horizontalalignment="center")


##
def val_iter(R, discount, maxSteps, infHor, probModel=None):
    """
   The val_iter function takes as input the reward matrix R, the discount factor discount,
   the maximum number of steps maxSteps and a boolean variable infHor that indicates whether
   or not we are using an infinite horizon problem. The function returns two matrices: V and A.
   The first one contains the value function for each state at each step computed through backwards induction.
   The second one contains the optimal actions (0 to 4) for each state at each step.
   So there are 5 different possible actions.

   :param R: A numpy array of shape (9, 10) which contains the reward for being in each state
   :param discount: A float discount factor
   :param maxSteps: An integer parametrizing the time-horizon
   :param infHor: A boolean parameterizing whether the horizon is infinite
   :param probModel: An (optional) numpy array of shape (5, 5) which contains the probability of executing action_ex when choosing action a_choice
   :return:
    - V - A numpy array of shape (9, 10, maxSteps) or ((9, 10) in inf horizon) containing the optimal value of each state at each time-step
    - A - A numpy array of shape (9, 10, maxSteps) or ((9, 10) in inf horizon) containing the optimal action for each state at each time-step
   """

    if probModel is None:
        probModel = np.eye(5)

    if not infHor:
        V = np.zeros((9, 10, maxSteps + 1))
        V[:, :, maxSteps] = R
        A = np.zeros((9, 10, maxSteps))
        for t in reversed(range(maxSteps)):
            # s a
            Q = np.zeros((9, 10, 5))
            Vp = np.pad(V, pad_width=((0, 1), (0, 1), (0, 0)), mode='constant', constant_values=-1e10)
            for m in range(9):
                for n in range(10):
                    Va = [Vp[m+1, n, t+1], Vp[m, n+1, t+1], Vp[m-1, n, t+1], Vp[m, n-1, t+1], Vp[m, n, t+1]]
                    Q[m,n,:] = R[m, n] + probModel@Va
                    # Q[m,n,0] = R[m, n] + np.sum(probModel[0]*Va)
                    # Q[m,n,1] = R[m, n] + np.sum(probModel[1]*Va)
                    # Q[m,n,2] = R[m, n] + np.sum(probModel[2]*Va)
                    # Q[m,n,3] = R[m, n] + np.sum(probModel[3]*Va)
                    # Q[m,n,4] = R[m, n] + np.sum(probModel[4]*Va)

            V[:,:,t] = np.max(Q, axis=-1)
            A[:,:,t] = np.argmax(Q, axis=-1)
        V = V[:,:,:-1]
    else:
        V = R
        A = np.zeros((9, 10))
        while True:
            Q = np.zeros((9, 10, 5))
            for m in range(9):
                for n in range(10):
                    Vp = np.pad(V, pad_width=((0,1),(0,1)), mode='constant', constant_values=-1e10)
                    # down, right, up, left, stay
                    Va = np.array([Vp[m+1, n], Vp[m, n+1], Vp[m-1, n], Vp[m, n-1], Vp[m, n]])
                    Q[m, n, :] = R[m, n] + discount*Va

            Vnew = np.max(Q, axis=-1)

            if (V == Vnew).all():
                break
            V = Vnew

        A = np.argmax(Q, axis=-1)
    return V, A


##
def max_action(V, R, discount, probModel=None):
    """
    The max_action function takes in the value function of the next step,
    the reward matrix, and a discount factor discount. It returns two matrices: V_star and a_star.
    The first one contains values at the next step and the second one contains
    optimal actions for each state at the current step. The value function is calculated by taking
    the maximum over actions of the discounted q-value.
    The optimal policy is taken by choosing the action that maximizes this quantity.

    :param V: matrix of shape (9, 10) which contains the optimal value of each step at the next step
    :param R: matrix of shape (9, 10) which contains the reward for being in each state at the current step
    :param discount: real discount factor
    :param probModel: (optional) matrix of shape (5, 5) which contains the probability of executing action_ex when choosing action a_choice
    :return:
    - V_star - A numpy array of shape (9, 10) containing the value of the current step
    - A_star - A numpy array of shape (9, 10) containing the action for the current step
    """
    # TODO YOUR CODE HERE
    V_star = np.zeros((9, 10))
    a_star = np.zeros((9, 10))
    return V_star, a_star


##
def find_policy(V, probModel=None):
    """
    The findPolicy function takes in a value function V and outputs the policy matrix.
    The policy matrix is filled with actions that are possible from each state.
    If an action is not possible, then it will be assigned as (0, 0) which means stay.

    :param V: Store the values of each state
    :param probModel: Pass the transition model in
    :return: The policy matrix that matches the optimal value function
    """
    # TODO YOUR CODE HERE
    pass


############################

save_figures = True

data = gen_grid_world()
grid_world = data[0] # ndarray where each row corresponds to a row of the grid world
grid_dict = data[1] # dictionary mapping values that are present in grid_world to strings

# TODO YOUR CODE HERE
# down, right, up, left, stay
prob_model = np.array([[.7, .1, 0, .1, .1],
                       [.1, .7, .1, 0, .1],
                       [0, .1, .7, .1, .1],
                       [.1, 0, .1, .7, .1],
                       [0, 0, 0, 0, 1]])

ax = show_world(grid_world, "Environment")
show_text_state(grid_world, grid_dict, ax)
if save_figures:
    plt.savefig("gridworld.pdf")

# TODO Finite Horizon
# with T=9, the bot stays at 8,3
T = 15 # horizon
# argmax deterministically picks down if all options equal
values, policy = val_iter(grid_world, 1, T, infHor=False)

show_world(np.maximum(values[:, :, 0], 0), 'Value Function - Finite Horizon - ' + str(T) + ' Steps Left')
if save_figures:
    plt.savefig(f'value_Fin_{T}.pdf')

# ax = show_world(...)
ax = show_world(grid_world, 'Policy - Finite Horizon - ' + str(T) + ' Steps Left')
show_policy(policy[:, :, 0], ax)
if save_figures:
    plt.savefig(f'policy_Fin_{T}.pdf')


# TODO Infinite Horizon
# its greedier, 15 timesteps is enought to view the whole feld
GAMMA = .8 # discount factor
values, policy = val_iter(grid_world, GAMMA, T, infHor=True)

show_world(np.maximum(values, 0), 'Value Function - Infinite Horizon')
if save_figures:
    plt.savefig(f'value_Inf_{GAMMA}.pdf')

ax = show_world(grid_world, 'Policy - Infinite Horizon')
show_policy(policy, ax)
if save_figures:
    plt.savefig(f'policy_Inf_{GAMMA}.pdf')

# TODO Finite Horizon with Probabilistic Transition
values, policy = val_iter(grid_world, 1, T, infHor=False, probModel=prob_model)
# policy looks worse than before since even running into the wall by accident could cause a lot of harm
# which is why the robot doesnt take the direct route but rather avoids actions that have any chance of hitting the wall
# while trying to get near rewarding spots and if reached, would rather just stay there (even if dirt and not toy)

# 1.f) only exception is 4,7 directly next to the toy, since it has the option to keep chosing moves
# such that he would never hit the wall and in any case would either bring him to the toy
# or to 4,9 or 6,9 (both of those with very low probability)
# where there`d be a risk of hitting a wall again, so that he can just stay there instead
# the chance of getting to the toy is way higher by this policy, making it worth it and the other c
show_world(np.maximum(values[:,:,0], 0), 'Value Function - Finite Horizon with Probabilistic Transition')
if save_figures:
    plt.savefig(f'value_Fin_{T}_prob.pdf')

ax = show_world(grid_world, 'Policy - Finite Horizon with Probabilistic Transition')
show_policy(policy[:, :, 0], ax)
if save_figures:
    plt.savefig(f'policy_Fin_{T}_prob.pdf')
plt.show()
