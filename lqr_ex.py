import warnings

import matplotlib.pyplot as plt
import numpy as np
np.random.seed(42) # /!\ keep this seed unchanged

warnings.filterwarnings("ignore")


class LQR:
    def __init__(self):
        """
        The __init__ function is called automatically when a new instance of the class is created.
        The __init__ function can take arguments, but self is always the first one.
        Self is just a reference to the instance of the class. It's automatically passed in when you instantiate an instance of the class.

        :param self: Refer to an instance of the class
        :return: None
        """
        # TODO set up all parameters here
        self.dim = 2  # number of dimensions of the state space
        self.T = 100 # time-horizon
        self.A = np.array([[1,0.1],[0,1]]) # state matrix
        self.B = np.array([[0],[0.1]]) # input matrix
        self.Sigma = np.diag([.1,.1]) # covariance matrix of transition noise
        self.b = np.array([10,0]) # mean of transition noise
        self.K = np.array([5, 0.3]) # controller gain
        self.k = 0.3 # controller offset
        self.H = 5 # input cost matrix (can be time-dependent but is fixed here)
        self.R = lambda t: np.diag([100, 1]) if t <= 50 else np.diag([1, 100])  # state cost matrix (can be time-dependent)
        self.r = lambda t: np.array([[50], [0]]) if 0 <= t <= 25 or 75 <= t <= 100 else np.array([[0], [50]]) # target state (can be time-dependent)
        self.N = 20  # number of experiments (it is important to run multiple experiments because the system is stochastic)

    def run(self, policy, rand_seed=None):
        """
        The run function runs the LQR with the given properties.
        It returns a tuple of three elements: (state, action, reward).
        The state is a 3D array of shape (2, 51, N), where 2 is the number of states in each time step and N is the number
        of experiments. The action and reward are both 3D arrays with shapes (51,) and (51,) respectively.

        :param self: Access variables that belongs to the class
        :param policy: Compute the action
        :param rand_seed=None: Ensure that the random seed is not set
        :return: [numpy arrays] The history of states (2, 51, 20), actions (1, 51, 20) and rewards (1, 51, 20) for each experiment
        """
        if rand_seed is not None:
            np.random.seed(rand_seed)
        history_s = np.zeros((2, self.T + 1, self.N))  # state at t=0 up to state at t=T for each experiment
        history_r = np.zeros((1, self.T + 1, self.N))  # reward at t=0 up to reward at t=T for each experiment
        history_a = np.zeros((1, self.T + 1, self.N))  # action at t=0 up to action at t=T for each experiment
        # TODO: implement the LQR with the given properties [for a and b]

        def reward(s, a, t):
            cost = 0 if t == self.T else a*self.H*a
            M = -np.sum((s-self.r(t))*(self.R(t)@(s-self.r(t))), axis=0)[None,:]
            return M - cost

        history_s[:,0,:] = np.random.multivariate_normal(mean=(0,0), cov=np.eye(2), size=self.N).T
        for t in range(self.T):
            s = history_s[:,t,:]
            a = policy(s, t)
            r = reward(s, a, t)

            history_a[:,t,:] = a
            history_r[:,t,:] = r

            w = np.random.multivariate_normal(mean=self.b, cov=self.Sigma, size=self.N).T
            history_s[:,t+1,:] = self.A@s + self.B@a + w

        a = policy(history_s[:, self.T, :], self.T)
        r = reward(history_s[:, self.T, :], a, self.T)
        history_a[:, self.T, :] = a
        history_r[:, self.T, :] = r

        return (history_s, history_a, history_r)

    def learn(self):
        """
        The learn function returns a policy function which takes in an input state and time,
        and returns the action to take. The policy is computed using the optimal LQR algorithm.

        :param self: Access variables that belongs to the class
        :return: A function that takes as input a state and time
        """
        # TODO: implement the optimal LQR [for task c]
        """
        implement your code here
        """

        V = np.zeros((self.T+1, 2, 2))
        v = np.zeros((self.T+1, 2, 1))
        V[self.T] = self.R(self.T)
        v[self.T] = self.R(self.T)@self.r(self.T)
        K = np.zeros((self.T+1, 1, 2))
        k = np.zeros((self.T+1, 1))

        for t in reversed(range(self.T)):
            K[t] = -np.linalg.inv(self.H+self.B.T@V[t+1]@self.B)@self.B.T@V[t+1]@self.A
            k[t] = -np.linalg.inv(self.H+self.B.T@V[t+1]@self.B)@self.B.T@(V[t+1]@self.b.reshape((2,1)) - v[t+1])
            M = self.B@np.linalg.inv(self.H+self.B.T@V[t+1]@self.B)@self.B.T@V[t+1]@self.A
            V[t] = self.R(t)+(self.A-M).T@V[t+1]@self.A
            v[t] = self.R(t)@self.r(t)+(self.A-M).T@(v[t+1]-V[t+1]@self.b.reshape((2,1)))

        def policy(s, t):
            # print((-K[t] @ s + k[t]).reshape(1, self.N))
            return ((K[t] @ s) + k[t]).reshape(1, self.N)
        return policy


def run_tasks(obj):
    """
    The run_tasks function runs the LQR, P controller and optimal policy.
    It plots the results for each task in a separate figure.

    :param obj: Pass the object to the run_tasks function
    :return: The mean and std of the cumulative reward over all experiments
    """
    # TODO: [task a] Run LQR controller and plot states and action and print the cumulative reward
    def plot(title, history_s, history_a, history_r):
        def plot_mean_and_ci(ts, c, z, label, plot_desired=True):
            ts = ts[c,:,:]
            x = np.arange(ts.shape[0])
            mean = np.mean(ts, axis=-1)
            std = np.std(ts, axis=-1)
            line, = plt.plot(x, mean, label=f"{label}{c}")
            plt.fill_between(x, mean-std*z, mean+std*z, alpha=.5)
            if plot_desired:
                plt.plot(x, [obj.r(t)[c] for t in x], linestyle='dashed', label=f"{label}{c}_des", color=line.get_color())

        plt.figure(100)
        plt.ylim(-400, 200)
        plot_mean_and_ci(history_s, 0, 1.96, "s")
        plot_mean_and_ci(history_s, 1, 1.96, "s")
        plt.legend()
        plt.title(title)
        plt.figure(101)
        plt.ylim(-600, 600)
        plot_mean_and_ci(history_a, 0, 1.96, "a", plot_desired=False)
        plt.legend()
        plt.title(title)
        print(f"mean cumulative reward for {title} : {np.mean(np.sum(history_r[0,:,:], axis=0), axis=0)}")
        print(f"std cumulative reward for {title} : {np.std(np.sum(history_r[0,:,:], axis=0), axis=0)}")

    def plot_ex(ex,a, b=None, c=None):
        def plot_mean_and_ci(ts, label, color, z=1.96):
            for c in range(ts.shape[0]):
                tss = ts[c, :, :]
                x = np.arange(tss.shape[0])
                mean = np.mean(tss, axis=-1)
                std = np.std(tss, axis=-1)
                if c == 0:
                    plt.plot(x, mean, label=f"{label}", color=color)
                else:
                    plt.plot(x, mean, color=color)
                plt.fill_between(x, mean-std*z, mean+std*z, alpha=.3, color=color)

        # the system heavily oscillates. it behaves like a p controller trying to
        # get to setpoint 0
        # the cumulative reward is bad, because the controller tries to converge to 0
        # but the reward is dependent on the squared distance to setpoints
        # r_t = (50, 0) and (0, 50)
        #
        if ex == "a":
            history_s, history_a, history_r = a
            plt.figure()
            plt.ylim(-400, 200)
            plot_mean_and_ci(history_s,  "a", "blue")
            plt.title("a) states")
            plt.figure()
            plt.ylim(-600, 600)
            plot_mean_and_ci(history_a,  "a", "blue")
            plt.title("a) actions")
            print(f"mean cumulative reward for: {np.mean(np.sum(history_r[0,:,:], axis=0), axis=0)}")
            print(f"sd cumulative reward for: {np.std(np.sum(history_r[0,:,:], axis=0), axis=0)}")
        # It oscillates heavy like in a),
        # but there is a slight shift upwards in the state graph which shows,
        # that this P controller is trying to reach the setpoints r_t= (50,0) and (0,50)
        # One can especially tell in the respective actions graph,
        # that it reacts according to the changing desired states at timesteps t=25 and t=75.
        # The cumulative reward is a lower as well (though not significantly,
        # since it`s still a terrible constant controller) because this controller
        # aims to the same setpoints, which the reward function is taking into consideration as well
        elif ex == "b":
            hist_s_a, hist_a_a, hist_r_a = a
            hist_s_b, hist_a_b, hist_r_b = b
            plt.figure()
            plt.ylim(-400, 200)
            plot_mean_and_ci(hist_s_a,  "a", "black")
            plot_mean_and_ci(hist_s_b,  "b", "red")
            plt.title("b) states")
            plt.legend()
            plt.figure()
            plt.ylim(-600, 600)
            plot_mean_and_ci(hist_a_a,  "a", "black")
            plot_mean_and_ci(hist_a_b,  "b", "red")
            plt.legend()
            plt.title("b) actions")

        # The Reward changes at timestep t=50, changing the importance
        # of each coordinate of the state error (s_t-r_t). For t <= 50,
        # the weights of R_t push for a low error in the first coordinate
        # with not much significance for the error in the second coordinate,
        # then for t > 50 the other way around.
        # Therefore in the first half, s_t[0] is approximating r_t[0] very well
        # and s_t[1] is off by a lot, and the other way around in the second half.

        # As for the actions, we see high values in the beginning of each timeframe [0,25],[25,50]...
        # till it reaches the desired state r_t(for the respective important coordinate)
        # and quickly moves back to an almost constant value that stabilizes the state for the rest of the timeframe
        elif ex == "c":
            hist_s_a, hist_a_a, hist_r_a = a
            hist_s_b, hist_a_b, hist_r_b = b
            hist_s_c, hist_a_c, hist_r_c = c
            plt.figure()
            plt.ylim(-400, 200)
            plot_mean_and_ci(hist_s_a,  "a", "black")
            plot_mean_and_ci(hist_s_b,  "b", "red")
            plot_mean_and_ci(hist_s_c,  "c", "green")
            plt.title("c) states")
            plt.legend()
            plt.figure()
            plt.ylim(-600, 600)
            plot_mean_and_ci(hist_a_a,  "a", "black")
            plot_mean_and_ci(hist_a_b,  "b", "red")
            plot_mean_and_ci(hist_a_c,  "c", "green")
            plt.legend()
            plt.title("c) actions")


    def policy_a(s, t):
        # s is (2,n)
        # return is (1,n)
        return (-obj.K@s + obj.k).reshape(1,obj.N)
        # return np.zeros((1,obj.N))

    plot_ex("a",obj.run(policy_a))


    """
    implement your code here
    """

    # TODO: [task b] Run P controller (s_des = r) and (s_des = 0) and plot the results
    def policy_b(s, t):
        return (obj.K @ (obj.r(t) - s) + obj.k).reshape(1, obj.N)
    plot_ex("b",obj.run(policy_a), obj.run(policy_b))
    # plt.plot(np.arange(obj.T+1), [obj.r(x)[0] for x in np.arange(obj.T+1)])
    # plt.plot(np.arange(obj.T+1), [obj.r(x)[1] for x in np.arange(obj.T+1)])
    # plt.show()

    """
    implement your code here
    """

    # TODO: [task c] Learn optimal policy and use it and plot the results
    """
    implement your code here
    """
    plot_ex("c", obj.run(policy_a), obj.run(policy_b), obj.run(obj.learn()))
    plt.show()


if __name__ == "__main__":
    LQR = LQR()
    run_tasks(LQR)
